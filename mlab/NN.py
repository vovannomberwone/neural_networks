import os
import math
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy
import pandas
import pylab
import copy
import torch.nn as nn
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore")

'''
def RB_func(x):
	loss_f = nn.MSELoss(reduction = 'sum')
	x_len = x.size(x.dim - 1)
	loss = x
	loss.zero_()
	if (x.dim() == 3):
		self.av = torch.Tensor(torch.zeros(x.size(0), self.b_size, x_len),
								requires_grad = True)
		self.val = torch.Tensor(torch.ones(x.size(0)), requires_grad = True)
		for i in range(x.size(0)):
			for j in range(x.size(1)):
				loss[i] += loss_f(x[i][j], av[i][j]) / val[i]
			loss[i] //= x.size(1)
		return loss
	elif (x.dim == 2):
		av = torch.FloatTensor(torch.zeros(x.size(0), x.size(1), x_len),
								requires_grad = True)
		val = torch.FloatTensor(torch.ones(1), requires_grad = True)
		for i in range(x.size(0)):
			loss[i] = loss_f(x[i], av[i]) / val[0]
		return loss
	else:
		print("RB_func:: error: wrong type of input:\n Expected tensor")
		print("dim 2 or 1 but dim:", x.dim - 1, "is detected")
		raise
'''
def identical_func(x):
	return x

class Net(nn.Module):
	def __init__(self, par):
		super(Net, self).__init__()
		self.par = par
		self.fcs = []
		self.bns = []
		for i in range(par.N_hidden):
			fc = nn.Linear(par.fc_sizes[i], par.fc_sizes[i + 1])
			setattr(self, 'fc%i' % i, fc)
			self.fcs.append(fc)
			if (par.do_bn and i < par.N_hidden - 1):
				bn = nn.BatchNorm1d(par.fc_sizes[i + 1])
				setattr(self, 'bn%i' % i, bn)
				self.bns.append(bn)
		
	def forward(self, x):
		for i in range(self.par.N_hidden):
			x = self.fcs[i](x)
			if (self.par.do_bn) and (i < self.par.N_hidden - 1):
				x = self.bns[i](x)
			x = self.par.act_funcs[i](x)
		return x

	def __str__(self):
		str1 = ''
		str2 = ''
		str3 = ''
		str_res = ''
		for i in range(self.par.N_hidden):
			str1 += '{}\n'.format(self.fcs[i])
			str2 += 'Weights Layer fc{}\n{}\n'.format(i, self.fcs[i].weight)
			str2 += 'Bias Layer fc{}\n{}\n'.format(i, self.fcs[i].bias)
			if (self.par.do_bn and i < self.par.N_hidden - 1):
				str2 += 'Weights Layer bn{}\n{}\n'.format(i, self.bns[i].weight)
				str2 += 'Bias Layer bn{}\n{}\n'.format(i, self.bns[i].bias)
			str3 += '{}\n'.format(self.par.act_funcs[i])
		str_res = 'Net_parameters:\n' + \
			'Do batchnormalization: {}\n'.format(self.par.do_bn) + \
			'Net structure:\n' + str1 + \
			'Batch size: {}\n'.format(self.par.b_size) + \
			'Activation functions:\n'+ str3 + \
			'Learning rate: {}\n'.format(self.par.learning_rate) + \
			'Momentum: {}\n'.format(self.par.mmm) + \
			'Epochs: {}\n'.format(self.par.epochs) + \
			'Loss funcion: {}\n'.format(self.par.loss_func) + \
			'Weights and bias of layers:\n' + str2
		return str_res

	def TRAIN(self, dataloader_train, dataloader_val):
		l_train_epochs = []
		l_val_epochs = []
		val_set_len_in_bch = len(dataloader_val)
		
		optimizer = torch.optim.SGD(
			self.parameters(), lr = self.par.learning_rate,
			momentum = self.par.mmm
		)
		
		#optimizer = torch.optim.Adagrad(self.parameters(), lr = self.par.learning_rate)
		
		for epoch in range(self.par.epochs): 
			self.train()
			for batch_idx, sample in enumerate(dataloader_train):
				optimizer.zero_grad()
				data = sample['x']
				target = sample['y']
				net_out = self(data).reshape(1, -1)
				loss = self.par.loss_func(net_out[0], target)
				loss.backward()
				optimizer.step()
			l_train_epochs.append(loss.item())
			
			# freeze parameter
			self.eval()
			loss_val = 0.0
			abs_func = nn.L1Loss(reduction = 'mean')
			mean = 0.0
			for batch_idx, sample in enumerate(dataloader_val):
				data = sample['x']
				target = sample['y'].reshape(1, -1)
				mean += abs_func(target[0], torch.zeros(1, self.par.b_size))
				net_out = self(data).reshape(1, -1)
				loss_val += self.par.loss_func(net_out[0], target[0])
			loss_val = loss_val / val_set_len_in_bch
			l_val_epochs.append(loss_val.item())
			print(
				"Epoch: {:3.0f}".format(epoch + 1),
				"\tLoss_Train:\t{:.10f}".format(loss.item()), 
				"\tLoss_Val:\t{:.10f}".format(loss_val.item())
			)
		mean = float(mean) / val_set_len_in_bch
		return [l_train_epochs, l_val_epochs, mean]
	
class FaceDataset(Dataset):
    def __init__(self, csv_file):
        self.frame = pandas.read_csv(csv_file)

    def __len__(self):
        return len(self.frame)

    def __getitem__(self, idx):
        y = self.frame.iloc[idx, 4]
        x = self.frame.iloc[idx, 0:4]
        sample = {'x': torch.tensor(x), 'y': torch.tensor(y)}
        return sample

class Params():
	def __init__(
		self, do_bn, fc_sizes, b_size, learning_rate, mmm, epochs,
		loss_func, act_funcs
	):
		self.do_bn = do_bn
		self.N_hidden = len(fc_sizes) - 1
		self.fc_sizes = fc_sizes
		self.b_size = b_size
		self.learning_rate = learning_rate
		self.mmm = mmm
		self.epochs = epochs
		self.loss_func = loss_func
		self.act_funcs = act_funcs

# plot y(x)
def plot_custom1(xlist, ylist, path):
	plt.plot(xlist, ylist)
	for i in range(len(ylist)):
		plt.scatter(xlist[i], ylist[i], c = 'blue')
	plt.grid()
	plt.savefig(path)
	plt.clf()

# plot y1(x1) and y2(x2)
def plot_custom2(xlist1, ylist1, xlist2, ylist2, path):
	plt.plot(xlist1, ylist1)
	plt.plot(xlist2, ylist2)
	for i in range(len(ylist1)):
		plt.scatter(xlist1[i], ylist1[i], c = 'red')
	for i in range(len(ylist2)):
		plt.scatter(xlist2[i], ylist2[i], c = 'blue')
	plt.grid()
	plt.savefig(path)
	plt.clf()

# plot y(x) only points
def plot_custom3(xlist, ylist, path):
	for i in range(len(ylist)):
		plt.scatter(xlist[i], ylist[i], c = 'blue', s = 1)
	plt.grid()
	plt.savefig(path)
	plt.clf()

def main():
	par = Params(
		do_bn = True, fc_sizes = [4, 10, 10, 10, 1],
		b_size = 100, learning_rate = 0.1, mmm = 0.80, epochs = 50,
		loss_func = nn.L1Loss(reduction = 'mean'),
		act_funcs = [torch.relu, torch.relu, torch.relu, torch.relu]
	)
	dataset_train = FaceDataset(csv_file = 'data/train_new.csv')
	dataloader_train = DataLoader(
		dataset_train, batch_size = par.b_size, shuffle = True, num_workers = 0,
		drop_last = True
	)
	dataset_val = FaceDataset(csv_file = 'data/validation_new.csv')
	dataloader_val = DataLoader(
		dataset_val, batch_size = par.b_size, shuffle = False, num_workers = 0,
		drop_last = True
	)

	net = Net(par)
	[l_train, l_val, mean] = net.TRAIN(dataloader_train, dataloader_val)	

	# create folder for results
	dirs_list = os.listdir('./res')
	path = './res/%s' % len(dirs_list)
	os.makedirs(path)
	
	# plot of Loss(epochs)
	my_fig = plt.figure()

	xlist = mlab.frange(1, par.epochs, 1)
	plot_custom2(
		xlist, l_val, xlist, l_train, path + '/LossTrainVal.jpg'
	)
	
	# plot functions f_net(8, 3, 0, x) and f_real(8, 3, 0, x) 
	# near optimum (8, 3, 0, 1) with step 0.1
	y_real = [
		6.1171, 6.1217, 6.1253, 6.1277, 6.1295, 6.1304, 6.1304, 6.1295, 
		6.1277, 6.125, 6.1214, 6.1168, 6.1114, 6.1051, 6.0979, 6.0898,
		6.0808, 6.0709, 6.0602, 6.0485, 6.036
	]
	xlist = mlab.frange(0.0, 2.0, 0.1)
	data = torch.zeros(len(y_real), 4)
	for i in range(0, len(y_real)):
		data[i] = torch.tensor([8, 3, 0, 0 + i * 0.1])
	y_net = net(data).reshape(1, -1)[0]
	plot_custom2(xlist, y_net.detach().numpy(), xlist, y_real,
		path + '/NearOptim(8,3,0,1)_short.png'
	)

	# plot functions f_net(8, 3, 0, x) and f_real(8, 3, 0, x) 
	# near optimum (8, 3, 0, 1) with step 1.0
	y_real = [
		6.1171, 6.1214, 6.036, 5.8635, 2.9292, 2.766, 2.5715, 2.3525,
	 	2.117, 2.0011, 1.7364, 1.0595, 0.89884, 0.76839, 0.7122,
	 	0.50462, 0.37021, 0.27698, 0.22509,	0.19238 ,0.17205
	]
	xlist = mlab.frange(0.0, 20.0, 1.0)
	for i in range(0, len(y_real)):
		data[i] = torch.tensor([8, 3, 0, 0 + i * 1.0])
	y_net = net(data).reshape(1, -1)[0]
	plot_custom2(xlist, y_net.detach().numpy(), xlist, y_real,
		path + '/NearOptim(8,3,0,1)_long.png'
	)

	# plot y_net(y_real)
	set_len_in_bch = len(dataloader_train) + len(dataloader_val)
	y_real = torch.tensor(set_len_in_bch, par.b_size)
	y_net = torch.tensor(set_len_in_bch, par.b_size)

	x_lin = mlab.frange(0.0, 6.0, 3.0)
	dataloader = DataLoader(
		dataset_train, batch_size = par.b_size, shuffle = False,
		num_workers = 0, drop_last = True
	)
	sz = 0
	for batch_idx, sample in enumerate(dataloader):
		data = sample['x']
		y_net[batch_idx] = net(data).reshape(1, -1)
		y_real[batch_idx] = sample['y'].reshape(1, -1)
		sz = sz + 1
	dataloader = DataLoader(
		dataset_val, batch_size = par.b_size, shuffle = False, num_workers = 0,
		drop_last = True
	)
	for batch_idx, sample in enumerate(dataloader):
		data = sample['x']
		y_net[sz + batch_idx] = net(data).reshape(1, -1)
		y_real[sz + batch_idx] = sample['y'].reshape(1, -1)
	plt.plot(x_lin, x_lin)
	y_net =  y_net.reshape(1, -1)
	y_real = y_real.reshape(1, -1)
	plot_custom3(
		y_real[0].numpy(), y_net[0].detach().numpy(),
		path + '/Linear_plot.png'
	)

	# save parameters
	f = open(path + '/parameters.txt', 'w')
	print('Mean, Relative error on last sample:', file = f)
	print(mean, ',', l_val[-1] / mean, file = f)
	print(net, file = f)
	f.close()

if __name__=='__main__':
	main()