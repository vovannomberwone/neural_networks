import os
import math
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy
import pandas
import pylab
import torch.nn as nn
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter

import warnings
warnings.filterwarnings("ignore")

def identical_func(x):
	return x

class FCNet(nn.Module):
	def __init__(self, par):
		super(FCNet, self).__init__()
		self.par = par
		self.fcs = []
		self.bns = []
		for i in range(par.N_hidden):
			fc = nn.Linear(par.fc_sizes[i], par.fc_sizes[i + 1])
			setattr(self, 'fc%i' % i, fc)
			self.fcs.append(fc)
			if (par.do_bn and i < par.N_hidden - 1):
				bn = nn.BatchNorm1d(par.fc_sizes[i + 1])
				setattr(self, 'bn%i' % i, bn)
				self.bns.append(bn)
		
	def forward(self, x):
		for i in range(self.par.N_hidden):
			x = self.fcs[i](x)
			if (self.par.do_bn) and (i < self.par.N_hidden - 1):
				x = self.bns[i](x)
			x = self.par.act_funcs[i](x)
		return x

	def TRAIN(self, dataloader_train, dataloader_val, writer):
		val_set_len_in_bch = len(dataloader_val)
		
		optimizer = torch.optim.SGD(
			self.parameters(), lr = self.par.learning_rate,
			momentum = self.par.mmm
		)
		#optimizer = torch.optim.Adagrad(self.parameters(), lr = self.par.learning_rate)
		
		for epoch in range(self.par.epochs): 
			self.train()
			for batch_idx, sample in enumerate(dataloader_train):
				optimizer.zero_grad()
				data = sample['x']
				target = sample['y']
				net_out = self.forward(data).reshape(1, -1)[0]
				loss_train = self.par.loss_func(net_out, target)
				loss_train.backward()
				optimizer.step()
			if loss_train.item() > 100:
				loss_train = 100
			else:
				loss_train = loss_train.item()
			
			# freeze parameter
			self.eval()
			loss_val = 0.0
			abs_func = nn.L1Loss(reduction = 'mean')
			for batch_idx, sample in enumerate(dataloader_val):
				data = sample['x']
				target = sample['y']
				net_out = self.forward(data).reshape(1, -1)[0]
				loss_val += self.par.loss_func(net_out, target)
			loss_val = loss_val / val_set_len_in_bch
			if loss_val.item() > 100:
				loss_val = 100
			else:
				loss_val = loss_val.item()

			print(
				"Epoch: {:3.0f}".format(epoch + 1),
				"  Loss_Train:  {:.10f}".format(loss_train), 
				"  Loss_Val:   {:.10f}".format(loss_val)
			)
			writer.add_scalar('loss_train', loss_train, epoch, None)
			writer.add_scalar('loss_validation', loss_val, epoch, None)
	
class FaceDataset(Dataset):
	def __init__(self, csv_file):
		self.frame = pandas.read_csv(csv_file)
		self.mean = self.frame.mean()
		self.std = self.frame.std() 
		self.frame = (self.frame - self.mean) / self.std
		self.frame = self.frame.fillna(0)
		
	def getMean(self):
		return self.mean

	def getStd(self):
		return self.std

	def __len__(self):
		return len(self.frame)

	def __getitem__(self, idx):
		x = self.frame.iloc[idx, 0:2]
		y = self.frame.iloc[idx, 2]
		sample = {'x': torch.tensor(x), 'y': torch.tensor(y)}
		return sample

class Params():
	def __init__(
		self, do_bn, fc_sizes, b_size, learning_rate, mmm, epochs,
		loss_func, act_funcs
	):
		self.do_bn = do_bn
		self.N_hidden = len(fc_sizes) - 1
		self.fc_sizes = fc_sizes
		self.b_size = b_size
		self.learning_rate = learning_rate
		self.mmm = mmm
		self.epochs = epochs
		self.loss_func = loss_func
		self.act_funcs = act_funcs

def save_parameters(writer, parameters):
	writer.add_scalar('Batch size', parameters.b_size)
	writer.add_scalar('Learning rate', parameters.learning_rate)
	writer.add_scalar('Momentum', parameters.mmm)
	writer.add_scalar('Epochs', parameters.epochs)

# plot y_net(y_target)
def draw_net_and_target(writer, net, dataloader_train, dataloader_val):
	my_fig = plt.figure()
	x_lin = mlab.frange(-3.0, 3.0, 3.0)
	plt.plot(x_lin, x_lin)

	y_target = torch.Tensor(len(dataloader_train), net.par.b_size)
	y_net = torch.Tensor(len(dataloader_train), net.par.b_size)
	for batch_idx, sample in enumerate(dataloader_train):
		data = sample['x']
		y_net[batch_idx] = net.forward(data).reshape(1, -1)
		y_target[batch_idx] = sample['y'].reshape(1, -1)
	y_target = y_target.reshape(1, -1)[0]
	y_net = y_net.reshape(1, -1)[0].detach().numpy()
	for i in range(len(y_net)):
		plt.scatter(y_net[i], y_target[i], c = 'red', s = 1)

	y_target = torch.Tensor(len(dataloader_val), net.par.b_size)
	y_net = torch.Tensor(len(dataloader_val), net.par.b_size)
	for batch_idx, sample in enumerate(dataloader_val):
		data = sample['x']
		y_net[batch_idx] = net.forward(data).reshape(1, -1)
		y_target[batch_idx] = sample['y'].reshape(1, -1)
	y_target = y_target.reshape(1, -1)[0]
	y_net = y_net.reshape(1, -1)[0].detach().numpy()
	for i in range(len(y_net)):
		plt.scatter(y_net[i], y_target[i], c = 'green', s = 1)

	plt.grid()
	writer.add_figure('y_target(y_net)', my_fig)

# plot functions f_net(0, x2) and f_target(0, x2) with step 0.05
def draw_proected(writer, net, mean_train, std_train):
	dataset_y_x2 = FaceDataset(csv_file = 'data/y(0,x2).csv')
	mean = dataset_y_x2.getMean()
	std = dataset_y_x2.getStd()
	dataloader_y_x2 = DataLoader(
		dataset_y_x2,
		batch_size = len(dataset_y_x2),
		shuffle = False,
		num_workers = 0,
		drop_last = False
	)
	for batch_idx, sample in enumerate(dataloader_y_x2):
		data = sample['x']
		y_target = sample['y'].detach().numpy()
		y_net = net.forward(data).reshape(1, -1)[0].detach().numpy()
	
	xlist = mlab.frange(-4.0, 4.0, 0.05)
	my_fig = plt.figure()
	plt.plot(xlist, y_net)
	plt.plot(xlist, y_target)
	for i in range(len(y_net)):
		y_net[i] = y_net[i] * std[2] + mean[2]
		y_target[i] = y_target[i] * std[2] + mean[2]
		#plt.scatter(xlist[i], , c = 'green')
		#plt.scatter(xlist[i], , c = 'red')
	plt.plot(xlist, y_net)
	plt.plot(xlist, y_target)
	plt.grid()
	writer.add_figure('y_(0, x2)', my_fig)

def draw_embedding(writer, dataset_train, dataset_val):
	#all_x = numpy.array(len(dataset_val) + len(dataset_train), 2)
	#all_y = numpy.array(len(dataset_val) + len(dataset_train), 1)
	train_x = dataset_train.frame.iloc[:, 0:2].values
	train_y = dataset_train.frame.iloc[:, 2].values
	writer.add_embedding(train_x, train_y)

def main():
	fc_sizes_arr = [2, 10, 20, 30, 20, 10, 1]
	#fc_sizes_arr = [2, 600, 1]
	
	
	act_funcs_arr = [
		torch.relu, torch.relu, torch.relu, torch.relu,
		identical_func
	]
	#act_funcs_arr = [
	#	torch.atan, torch.atan, torch.atan, torch.atan,
	#	identical_func
	#]

	#act_funcs_arr = [torch.atan, identical_func]

	par = Params(
		do_bn = True,
		fc_sizes = fc_sizes_arr,
		b_size = 336,
		learning_rate = 0.02,
		mmm = 0.9,
		epochs = 70,
		loss_func = nn.MSELoss(reduction = 'mean'),
		act_funcs = act_funcs_arr
	)
	dataset_train = FaceDataset(csv_file = 'data/train.csv')
	dataloader_train = DataLoader(
		dataset_train,
		batch_size = par.b_size,
		shuffle = True,
		num_workers = 0,
		drop_last = True
	)
	dataset_val = FaceDataset(csv_file = 'data/validation.csv')
	dataloader_val = DataLoader(
		dataset_val,
		batch_size = par.b_size,
		shuffle = False,
		num_workers = 0,
		drop_last = True
	)

	writer = SummaryWriter()
	net = FCNet(par)
	net.TRAIN(dataloader_train, dataloader_val, writer)
	
	junk = next(iter(dataloader_train))['x']
	writer.add_graph(net, junk)
	save_parameters(writer, par)

	mean_train = dataset_train.getMean()
	std_train = dataset_train.getStd()
	draw_proected(writer, net, mean_train, std_train)
	draw_net_and_target(writer, net, dataloader_train, dataloader_val)
	#draw_embedding(writer, dataset_train, dataset_val)
	writer.close()

if __name__=='__main__':
	main()